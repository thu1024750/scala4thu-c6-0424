package apps

import cc.ListNode
/**
  * Created by mac045 on 2017/4/24.
  */
object ListNodeApp extends App{
  val listNode=ListNode(1,ListNode(2,ListNode(3,ListNode(4,null))))

  println(listNode)

}

object SizeApp extends App{
  val listNode1=ListNode(1,ListNode(2,ListNode(3,ListNode(4,null)))).size

  println(listNode1)
}

object FilterApp extends App{
  val listNode2=ListNode(1,ListNode(2,ListNode(3,ListNode(4,null)))).filter(3)

  println(listNode2)
}